# 一、python安装与环境搭建(Windows)

python网站：www.python.org

视频教学的python版本为 Python 3.10.4

我安装的python版本为 Python 3.12.0



注意：

1、安装时，下方的 Add python.exe to PATH 一定要勾选；

2、选择完安装选项和安装路径后，安装完成，点击下方：Disable path length limit



验证安装是否成功：打开cmd控制台，输入python回车即可。

# 二、pycharm编译器安装

下载地址：https://www.jetbrains.com/

注意：下载Community（社区版）就够了，专业版免费30天，后续收费。

# 三、pycharm编译器的使用

1、创建新项目时，需要选择 Previously configured interpreter，路径选择为安装python时的路径；

# 四、python基础语法

### 注释

```python
# 第一个注释
# 第二个注释
 
'''
第三注释
第四注释
'''
 
"""
第五注释
第六注释
"""
```

### 行与缩进

```python
if True:
    print ("Answer")
    print ("True")
else:
    print ("Answer")
  print ("False")    # 缩进不一致，会导致运行错误
```

### 多行语句

```python
# Python 通常是一行写完一条语句，但如果语句很长，我们可以使用反斜杠 \ 来实现多行语句，例如：
total = item_one + \
        item_two + \
        item_three

# 在 [], {}, 或 () 中的多行语句，不需要使用反斜杠 \，例如：
total = ['item_one', 'item_two', 'item_three',
        'item_four', 'item_five']
```

### 同一行显示多条语句

```python
import sys; x = 'runoob'; sys.stdout.write(x + '\n')
```

多个语句构成代码组

```python
# 缩进相同的一组语句构成一个代码块，我们称之代码组。
# 像if、while、def和class这样的复合语句，首行以关键字开始，以冒号( : )结束，该行之后的一行或多行代码构成代码组。
# 我们将首行及后面的代码组称为一个子句(clause)。
# 如下实例：
if expression : 
   suite
elif expression : 
   suite 
else : 
   suite
```

### print 输出

```python
x="a"
y="b"
# 换行输出
print( x )
print( y )
 
print('---------')
# print 默认输出是换行的，如果要实现不换行需要在变量末尾加上 end=""
# 不换行输出
print( x, end=" " )
print( y, end=" " )
```

### import 与 from...import

```python
# 在 python 用 import 或者 from...import 来导入相应的模块。
# 将整个模块(somemodule)导入，格式为： import somemodule
# 从某个模块中导入某个函数,格式为： from somemodule import somefunction
# 从某个模块中导入多个函数,格式为： from somemodule import firstfunc, secondfunc, thirdfunc
# 将某个模块中的全部函数导入，格式为： from somemodule import *
```

# 五、python基本数据类型

```python
# Python 3 中的数据类型：
# Number(数字)
# String(字符串)
# bool(布尔类型)
# List(列表)
# Tuple(元组)
# Set(集合)
# Dictionary(字典)

# Python3 的六个标准数据类型中：
# 不可变数据（3 个）：Number（数字）、String（字符串）、Tuple（元组）；
# 可变数据（3 个）：List（列表）、Dictionary（字典）、Set（集合）。
# 此外还有一些高级的数据类型，如: 字节数组类型(bytes)。
```

## Number 数字类型

Python3 支持 **int、float、bool、complex（复数）**。

```python
# int、float、bool、complex（复数）
# 内置的 type() 函数可以用来查询变量所指的对象类型。
a, b, c, d = 20, 5.5, True, 4+3j
print(type(a), type(b), type(c), type(d))
# 还可以使用isinstance来判断
f = 111
print(isinstance(f, float))
# isinstance 用来判断，第一个参数是否是第二个参数的子类
```

## String 字符串

Python中的字符串用单引号 **'** 或双引号 **"** 括起来，同时使用反斜杠 \ 转义特殊字符。

```python
str = '123456789'
print(str)  # 输出字符串
print(str[0:-1])  # 输出第一个到倒数第二个的所有字符 
# 字符串的截取的语法格式如下：变量[头下标:尾下标:步长]
print(str[0])  # 输出字符串第一个字符
print(str[2:5])  # 输出从第三个开始到第六个的字符（不包含）
print(str[2:])  # 输出从第三个开始后的所有字符
print(str[1:5:2])  # 输出从第二个开始到第五个且每隔一个的字符（步长为2）
print(str * 2)  # 输出字符串两次
print(str + '你好')  # 连接字符串
print('------------------------------')

print('hello\nrunoob')  # 使用反斜杠(\)+n转义特殊字符
print(r'hello\nrunoob')  # 在字符串前面添加一个 r，表示原始字符串，不会发生转义
```

## bool 布尔类型

在 Python 中，True 和 False 都是关键字，表示布尔值。

布尔类型特点：

布尔类型只有两个值：True 和 False。

布尔类型可以和其他数据类型进行比较，比如数字、字符串等。在比较时，Python 会将 True 视为 1，False 视为 0。

布尔类型可以和逻辑运算符一起使用，包括 and、or 和 not。这些运算符可以用来组合多个布尔表达式，生成一个新的布尔值。

布尔类型也可以被转换成其他数据类型，比如整数、浮点数和字符串。在转换时，True 会被转换成 1，False 会被转换成 0。

```python
aa = True
bb = False

# 比较运算符
print(2 < 3)  # True
print(2 == 3)  # False

# 逻辑运算符
print(aa and bb)  # False
print(aa or bb)  # True
print(not aa)  # False

# 类型转换
print(int(aa))  # 1
print(float(bb))  # 0.0
print(str(aa))  # "True"
print(type(str(aa)))  # <class 'str'>
```

##  List 列表

列表截取的语法格式如下：变量[头下标:尾下标]

索引值以 0 为开始值，-1 为从末尾的开始位置。

```python
list = ['abcd', 786, 2.23, 'runoob', 70.2]
tinylist = [123, 'runoob']

print(list)      # 输出完整列表
print(list[0])     # 输出列表第一个元素
print(list[1:3])    # 从第二个开始输出到第三个元素
print(list[2:])    # 输出从第三个元素开始的所有元素
print(tinylist * 2)  # 输出两次列表
print(list + tinylist)  # 连接列表
# 与字符串不一样的是，列表中的元素是可以改变的
list[0] = 'abcdefg'
print(list)
```

## Tuple 元组

元组（tuple）与列表类似，不同之处在于元组的元素不能修改。元组写在小括号 () 里，元素之间用逗号隔开。

```python
tuple = ('abcd', 786, 2.23, 'runoob', 70.2)
tinytuple = (123, 'runoob')

print(tuple)  # 输出完整元组
print(tuple[0])  # 输出元组的第一个元素
print(tuple[1:3])  # 输出从第二个元素开始到第三个元素
print(tuple[2:])   # 输出从第三个元素开始的所有元素
print(tinytuple * 2)   # 输出两次元组
print(tuple + tinytuple)   # 连接元组
# tuple[0] = 'lyh' # 修改元组元素的操作是非法的

# 构造包含 0 个或 1 个元素的元组比较特殊，所以有一些额外的语法规则：
tup1 = ()    # 空元组
tup2 = (20,)  # 一个元素，需要在元素后添加逗号
```

## Set 集合

Python 中的集合（Set）是一种无序、可变的数据类型，用于存储唯一的元素。

集合中的元素不会重复，并且可以进行交集、并集、差集等常见的集合操作。

在 Python 中，集合使用大括号 {} 表示，元素之间用逗号 , 分隔。

另外，也可以使用 set() 函数创建集合。

注意：创建一个空集合必须用 set() 而不是 { }，因为 { } 是用来创建一个空字典。

创建格式：

parame = {value01,value02,...}

或者

set(value)

```python
sites = {'Google', 'Taobao', 'Runoob', 'Facebook', 'Zhihu', 'Baidu', 'Baidu', 'baidu'}

print(sites)  # 输出集合，重复的元素被自动去掉

# 成员测试
if 'Runoob' in sites :
   print('Runoob 在集合中')
else :
   print('Runoob 不在集合中')


# set可以进行集合运算
a = set('abracadabra')
b = set('alacazam')

print(a)
print(a - b)   # a 和 b 的差集
print(b - a)   # b 和 a 的差集
print(a | b)   # a 和 b 的并集
print(a & b)   # a 和 b 的交集
print(a ^ b)   # a 和 b 中不同时存在的元素

```

## Dictionary 字典

字典（dictionary）是Python中另一个非常有用的内置数据类型。

列表是有序的对象集合，字典是无序的对象集合。两者之间的区别在于：字典当中的元素是通过键来存取的，而不是通过偏移存取。

字典是一种映射类型，字典用 { } 标识，它是一个无序的 键(key) : 值(value) 的集合。

键(key)必须使用不可变类型。

在同一个字典中，键(key)必须是唯一的。

```python
dict = {}
dict['one'] = "1 - 菜鸟教程"
dict[2] = "2 - 菜鸟工具"

tinydict = {'name': 'runoob', 'code': 1, 'site': 'www.runoob.com'}

print(dict['one'])    # 输出键为 'one' 的值
print(dict[2])      # 输出键为 2 的值
print(dict)  # 输出dict
print(tinydict)     # 输出完整的字典
print(tinydict.keys())  # 输出所有键
print(tinydict.values())  # 输出所有值
```

## bytes 类型

在 Python3 中，bytes 类型表示的是不可变的二进制序列（byte sequence）。

与字符串类型不同的是，bytes 类型中的元素是整数值（0 到 255 之间的整数），而不是 Unicode 字符。

bytes 类型通常用于处理二进制数据，比如图像文件、音频文件、视频文件等等。在网络编程中，也经常使用 bytes 类型来传输二进制数据。

```python
# 创建 bytes 对象的方式有多种，最常见的方式是使用 b 前缀：
# 此外，也可以使用 bytes() 函数将其他类型的对象转换为 bytes 类型。bytes() 函数的第一个参数是要转换的对象，第二个参数是编码方式，如果省略第二个参数，则默认使用 UTF-8 编码：
xx = bytes("hello", encoding="utf-8")

# 与字符串类型类似，bytes 类型也支持许多操作和方法，如切片、拼接、查找、替换等等。同时，由于 bytes 类型是不可变的，因此在进行修改操作时需要创建一个新的 bytes 对象。例如：
x = b"hello"
y = x[1:3]  # 切片操作，得到 b"el"
z = x + b"world"  # 拼接操作，得到 b"helloworld"
print(x)
print(y)
print(z)
# 需要注意的是，bytes 类型中的元素是整数值，因此在进行比较操作时需要使用相应的整数值。例如：
# x = b"hello"
# ord() 函数用于将字符转换为相应的整数值
if x[0] == ord("h"):
    print("The first element is 'h'")
    
print(ord("h"))
```